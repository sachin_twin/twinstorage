//
//  TwinOperation.swift
//  
//
//  Created by Sachin Rastogi on 3/18/21.
//

import Foundation

class TwinOperation: Operation{
    
}

class TwinWriteOperation: TwinOperation {
    let data: Data
    let url: URL
    let completion: TwinStorageWriteCompletion
    
    init(write to: URL, data:Data, completion: @escaping TwinStorageWriteCompletion) {
        self.data = data
        self.url = to
        self.completion = completion        
        super.init()
        
        //We want read operation higher priority than write
        self.queuePriority = .normal
    }
    
    override func main() {
        do {
            try data.write(to: self.url)
            completion(.success(true))
        }
        catch (let error){
            completion(.failure(.failure(error: error)))
        }
    }
}

class TwinReadOperation<T: Decodable>: TwinOperation {
    let url: URL
    let completion: TwinStorageReadCompletion<T>
    let type: T.Type

    init(from: URL, type: T.Type, completion: @escaping TwinStorageReadCompletion<T>) {
        self.url = from
        self.completion = completion
        self.type = type
        super.init()
        self.queuePriority = .high
    }
    
    override func main() {
        if let handler = FileHandle(forReadingAtPath: url.path){
            defer{
                handler.closeFile()
            }
            
            if #available(iOS 13.4, *){
                if let data = try? handler.readToEnd(){
                    if let obj = try? JSONDecoder().decode(self.type, from: data){
                        completion(.success(obj))
                    }
                    else{
                        completion(.failure(.incorrectDataType))
                    }
                }
                else{
                    completion(.failure(.noDataFound))
                }
            }
            else{
                let data = handler.readDataToEndOfFile()
                if let obj = try? JSONDecoder().decode(self.type, from: data){
                    completion(.success(obj))
                }
                else{
                    completion(.failure(.incorrectDataType))
                }
            }
        }
        else{
            completion(.failure(.fileNotFound))
        }
    }
}
