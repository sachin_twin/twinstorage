//
//  TwinStorage.swift
//
//
//  Created by Sachin Rastogi on 3/18/21.
//

import Foundation

/// Write Operation Completion Block
public typealias TwinStorageWriteCompletion = (Result<Bool, TwinStorageError>) -> Void

/// Read Operation Completion Block
public typealias TwinStorageReadCompletion<T: Decodable> = (Result<T, TwinStorageError>) -> Void

public enum TwinStorageError: Error {
    case fileNotFound
    case failure(msg: String)
    case failure(error: Error)
    case incorrectDataType
    case noDataFound
}

public enum TwinStorageFolder: String, CaseIterable {
    case reports
    case nutrition
    case foods
}

public class TwinStorage {

    private let opQueue: OperationQueue
    private let root: FileManager.SearchPathDirectory
    
    public init(root: FileManager.SearchPathDirectory = .cachesDirectory) {
        opQueue = OperationQueue()
        opQueue.maxConcurrentOperationCount = 2
        self.root = root
    }

}

typealias TwinStorageInfo = TwinStorage
extension TwinStorageInfo{

    public func getAllFilesName(from folder: TwinStorageFolder) -> [String]? {
        do {
            let docURL = try FileManager.default.url(for: self.root, in: .userDomainMask, appropriateFor: nil, create: false)
            let folderPath = docURL.appendingPathComponent(folder.rawValue)
            let contents = try FileManager.default.contentsOfDirectory(at: folderPath, includingPropertiesForKeys: [.fileResourceTypeKey], options: .skipsHiddenFiles)
            return contents.map({$0.lastPathComponent})
        } catch {
            //TODO: Logging
            return nil
        }
    }

    public func fileInfo(fileName file: String, in folder: TwinStorageFolder) -> [FileAttributeKey : Any]? {
        guard let fileUrl = url(file: file, folder: folder) else {
            return nil
        }
        do {
            let info = try FileManager.default.attributesOfItem(atPath: fileUrl.path)
            return info
        }
        catch {
            //TODO: Logging
            return nil
        }
    }
    
    public func url(file: String, folder: TwinStorageFolder) ->  URL? {
        guard let resourcePath = (FileManager.default.urls(for: self.root, in: .userDomainMask)).last else {
            //TODO: Logging
            return nil
        }
        
        let folderPath = resourcePath.appendingPathComponent(folder.rawValue, isDirectory: true)
        if !FileManager.default.fileExists(atPath: folderPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: folderPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                //TODO: Logging
                return nil
            }
        }
        
        return folderPath.appendingPathComponent(file)
    }

}

typealias TwinStorageIO = TwinStorage
extension TwinStorageIO{
    
    public func write(data: Data, to folder: TwinStorageFolder, fileName:String, onCompletion: @escaping TwinStorageWriteCompletion) {
        if let fileUrl = url(file: fileName, folder: folder){
            let op = TwinWriteOperation(write: fileUrl, data: data, completion: onCompletion)
            opQueue.addOperation(op)
        }
        else{
            onCompletion(.failure(.fileNotFound))
        }
    }

    public func read<T: Decodable>(file: String, from folder: TwinStorageFolder, type: T.Type,  onCompletion: @escaping TwinStorageReadCompletion<T>) {
        if let fileUrl = url(file: file, folder: folder){
            let op = TwinReadOperation(from: fileUrl, type: type, completion: onCompletion)
            opQueue.addOperation(op)
        }
        else{
            onCompletion(.failure(.fileNotFound))
        }
    }
    
}

typealias TwinStorageRemove = TwinStorage
extension TwinStorageRemove{
    
    public func removeFile(fileName file: String, from folder:TwinStorageFolder) -> Bool {
        guard let fileUrl = url(file: file, folder: folder) else {
            //return true as file is not present at all
            return true
        }
        do {
            try FileManager.default.removeItem(at: fileUrl)
            return true
        } catch {
            //TODO: Logging
            return false
        }
    }

    public func deleteAllTwinFolders() {
        do {
            let docURL = try FileManager.default.url(for: self.root, in: .userDomainMask, appropriateFor: nil, create: false)
            for folder in TwinStorageFolder.allCases{
                try? FileManager.default.removeItem(at: docURL.appendingPathComponent(folder.rawValue, isDirectory: true))
            }
        } catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
}
