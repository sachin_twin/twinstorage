import XCTest

import TwinStorageTests

var tests = [XCTestCaseEntry]()
tests += TwinStorageTests.allTests()
XCTMain(tests)
